FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

# renovate: datasource=github-releases depName=miniflux/v2 versioning=semver
ARG MINIFLUX_VERSION=2.2.6

RUN curl --fail -L https://github.com/miniflux/v2/releases/download/${MINIFLUX_VERSION}/miniflux-linux-amd64 -o /app/code/miniflux && \
    chmod +x /app/code/miniflux

COPY start.sh /app/code/

CMD [ "/app/code/start.sh" ]

