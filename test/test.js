#!/usr/bin/env node

/* jslint node:true */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const USERNAME = process.env.USERNAME;
    const PASSWORD = process.env.PASSWORD;

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function exists(selector) {
        await browser.wait(until.elementLocated(selector), TEST_TIMEOUT);
    }

    async function visible(selector) {
        await exists(selector);
        await browser.wait(until.elementIsVisible(browser.findElement(selector)), TEST_TIMEOUT);
    }

    async function loginAdmin() {
        await browser.get(`https://${app.fqdn}`);
        await visible(By.id('form-username'));
        await browser.findElement(By.id('form-username')).sendKeys('admin');
        await browser.findElement(By.id('form-password')).sendKeys('changeme');
        await browser.findElement(By.xpath('//button[contains(text(), "Login")]')).click();
        await browser.wait(until.elementLocated(By.xpath('//a[contains(text(), "Unread")]')), TEST_TIMEOUT);
    }

    async function loginOIDC(username, password, alreadyAuthenticated) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}`);
        await browser.sleep(4000);

        await visible(By.id('form-username'));

        await browser.findElement(By.xpath('//a[@href="/oauth2/oidc/redirect"]')).click();
        await browser.sleep(4000);

        if (!alreadyAuthenticated) {
            await visible(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.sleep(2000);
            await browser.findElement(By.id('loginSubmitButton')).click();
            await browser.sleep(2000);
        }

        await browser.wait(until.elementLocated(By.xpath('//a[contains(text(), "Unread")]')), TEST_TIMEOUT);
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/logout`);

        await visible(By.id('form-username'));
    }

    async function addFeed() {
        await browser.get('https://' + app.fqdn + '/subscribe');
        await browser.sleep(2000);
        await visible(By.id('form-url'));
        await browser.findElement(By.id('form-url')).sendKeys('https://blog.cloudron.io');
        await browser.findElement(By.xpath('//button[contains(text(), "Find a")]')).click();
        await browser.sleep(2000);
        await browser.wait(until.elementLocated(By.xpath('//a[contains(text(), "Cloudron") or contains(text(), "Self-hosting")]')), TEST_TIMEOUT);
    }

    async function checkFeed() {
        await browser.get('https://' + app.fqdn + '/feeds');
        await browser.wait(until.elementLocated(By.xpath('//a[contains(., "Cloudron")]')), TEST_TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    xit('build app', function () {
        execSync('cloudron build', EXEC_ARGS);
    });

    // no sso
    it('can install app (no sso)', function () {
        execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can Admin login', loginAdmin);
    it('can add feed', addFeed);
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    it('install app', function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can Admin login', loginAdmin);
    it('can add feed', addFeed);
    it('can logout', logout);

    it('can user login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, false));
    it('can add feed', addFeed);
    it('can check feed', checkFeed);
    it('can logout', logout);

    it('restart app', function () {
        execSync('cloudron restart --app ' + app.id, EXEC_ARGS);
    });

    it('can Admin login', loginAdmin);
    it('can check feed', checkFeed);
    it('can logout', logout);

    it('can user login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, true));
    it('can check feed', checkFeed);
    it('can logout', logout);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, EXEC_ARGS);
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can Admin login', loginAdmin);
    it('can check feed', checkFeed);
    it('can logout', logout);

    it('can user login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, true));
    it('can check feed', checkFeed);
    it('can logout', logout);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can Admin login', loginAdmin);
    it('can check feed', checkFeed);
    it('can logout', logout);

    it('can user login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, true));
    it('can check feed', checkFeed);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app', function () {
        execSync(`cloudron install --appstore-id ${app.manifest.id} --location ${LOCATION}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can Admin login', loginAdmin);
    it('can add feed', addFeed);
    it('can logout', logout);

    it('can user login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, true));
    it('can add feed', addFeed);
    it('can check feed', checkFeed);
    it('can logout', logout);

    it('can update', function () {
        execSync('cloudron update --app ' + app.id, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });

    it('can Admin login', loginAdmin);
    it('can check feed', checkFeed);
    it('can logout', logout);

    it('can user login OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, true));
    it('can check feed', checkFeed);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
