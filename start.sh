#!/bin/bash

[[ -f /app/data/env ]] && mv /app/data/env /app/data/env.sh

if [[ ! -f /app/data/env.sh ]]; then
    echo -e "# Add custom env variables here. See https://miniflux.app/docs/configuration.html\n#export BATCH_SIZE=50\n" > /app/data/env.sh
fi

source /app/data/env.sh

export DATABASE_URL="${CLOUDRON_POSTGRESQL_URL}?sslmode=disable"
export BASE_URL="${CLOUDRON_APP_ORIGIN}"
export ROOT_URL="${CLOUDRON_APP_ORIGIN}"
export HTTPS=true
export LISTEN_ADDR=0.0.0.0:3000
export RUN_MIGRATIONS=false # we run this manually below

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    echo "==> Enabling OIDC"
    # CLOUDRON_OIDC_PROVIDER_NAME is not supported
    export OAUTH2_PROVIDER=oidc
    export OAUTH2_CLIENT_ID=${CLOUDRON_OIDC_CLIENT_ID}
    export OAUTH2_CLIENT_SECRET=${CLOUDRON_OIDC_CLIENT_SECRET}
    export OAUTH2_REDIRECT_URL=${CLOUDRON_APP_ORIGIN}/oauth2/oidc/callback
    export OAUTH2_OIDC_DISCOVERY_ENDPOINT=${CLOUDRON_OIDC_ISSUER}
    export OAUTH2_USER_CREATION=1
fi

user_count=$(PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -XAwt -c "select count(*) from information_schema.tables WHERE table_schema = 'users';")

if [[ "${user_count}" == "0" ]]; then
    echo "==> Initializing miniflux on first run"

    /app/code/miniflux -migrate # also created hstore extension

    # miniflux -create-admin
    export ADMIN_USERNAME=admin
    export ADMIN_PASSWORD=changeme
    export CREATE_ADMIN=1
else
    echo "==> Running migrations"
    /app/code/miniflux -migrate
fi

chown -R cloudron:cloudron /app/data/

echo "==> Starting miniflux"
exec gosu cloudron:cloudron /app/code/miniflux

